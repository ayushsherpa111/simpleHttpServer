package server

import (
	"fmt"
	"net/http"
)

type simpleServer struct {
	router *http.Server
}

func (s *simpleServer) ListenAndServe() {
	fmt.Println("Listening on port ", s.router.Addr)
	s.router.ListenAndServe()
}

func New(port string, handler http.Handler) *simpleServer {
	return &simpleServer{
		router: &http.Server{
			Addr:    port,
			Handler: handler,
		},
	}
}
