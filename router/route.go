package router

type route struct {
	path    string
	methods map[string]handlerFunc
}

func (r *route) registerHandler(method string, handler handlerFunc) {
	r.methods[method] = handler
}

func newRoute(path string) *route {
	return &route{
		path:    path,
		methods: make(map[string]handlerFunc),
	}
}
