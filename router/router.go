package router

import (
	"fmt"
	"net/http"
)

type router struct {
	mux    *http.ServeMux
	routes map[string]*route
}

func (r *router) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	r.mux.ServeHTTP(w, req)
}

func (r *router) getExistingRouteOrNew(path string) *route {
	if r, ok := r.routes[path]; ok {
		return r
	}
	newRoute := newRoute(path)
	r.routes[path] = newRoute
	return newRoute
}

func (r *router) setMethod(pattern string, handler handlerFunc) {
	r.mux.HandleFunc(pattern, logRequests(handler))
}

func (r *router) Get(pattern string, handler handlerFunc) {
	route := r.getExistingRouteOrNew(pattern)
	route.registerHandler(http.MethodGet, handler)
}

func (r *router) Options(pattern string, handler handlerFunc) {
	route := r.getExistingRouteOrNew(pattern)
	route.registerHandler(http.MethodOptions, preflight(handler))
}

func (r *router) Post(pattern string, handler handlerFunc) {
	route := r.getExistingRouteOrNew(pattern)
	route.registerHandler(http.MethodPost, handler)
}

func (r *router) RegisterRoutes() {
	for path, varRoute := range r.routes {
		fmt.Println("Registered ", path)
		handler := func(rote *route) handlerFunc {
			return func(rw http.ResponseWriter, r *http.Request) {
				if handler, ok := rote.methods[r.Method]; ok {
					handler(rw, r)
				} else {
					rw.Header().Set("Content-Type", "application/json")
					rw.WriteHeader(http.StatusMethodNotAllowed)
					rw.Write([]byte(`{ error: "Invalid method in URI" }`))
				}
			}
		}(varRoute)
		r.setMethod(path, handler)
	}
}

func InitRouter() *router {
	r := &router{
		mux:    http.NewServeMux(),
		routes: make(map[string]*route),
	}

	return r
}
