package router

import (
	"fmt"
	"net/http"
)

const (
	originHeader = "Access-Control-Allow-Origin"
	credHeader   = "Access-Control-Allow-Credentials"
	methodHeader = "Access-Control-Allow-Methods"
	allowHeader  = "Access-Control-Allow-Headers"
)

type handlerFunc func(http.ResponseWriter, *http.Request)

func logRequests(handler handlerFunc) handlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		fmt.Println("Received request: ", r.URL.Path)
		fmt.Println("Method: ", r.Method)
		handler(rw, r)
	}
}

func preflight(handler handlerFunc) handlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		rw.Header().Set(originHeader, "http://localhost:3000")
		rw.Header().Set(credHeader, "true")
		rw.Header().Set(methodHeader, "POST,GET,OPTIONS,DELETE")
		rw.Header().Set(allowHeader, "*")

		handler(rw, r)
	}
}

func setHeaders(headerKey, headerValue string, handler handlerFunc) handlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		rw.Header().Set(headerKey, headerValue)
		handler(rw, r)
	}
}

func checkMethod(method string, handler handlerFunc) handlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		if r.Method != method {
			rw.Header().Set("Content-Type", "application/json")
			rw.WriteHeader(http.StatusMethodNotAllowed)
			rw.Write([]byte(`{ error: "Invalid method in URI" }`))
			return
		} else {
			handler(rw, r)
		}
	}
}

func json(handler handlerFunc) handlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		rw.Header().Set("Content-Type", "application/json")
		handler(rw, r)
	}
}
