package sentiment

import (
	reflect "reflect"
	sync "sync"

	proto "github.com/golang/protobuf/proto"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// This is a compile-time assertion that a sufficiently up-to-date version
// of the legacy proto package is being used.
const _ = proto.ProtoPackageIsVersion4

type SentimentReq struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Value string `protobuf:"bytes,1,opt,name=value,proto3" json:"value,omitempty"`
}

func (x *SentimentReq) Reset() {
	*x = SentimentReq{}
	if protoimpl.UnsafeEnabled {
		mi := &file_sentiment_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SentimentReq) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SentimentReq) ProtoMessage() {}

func (x *SentimentReq) ProtoReflect() protoreflect.Message {
	mi := &file_sentiment_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SentimentReq.ProtoReflect.Descriptor instead.
func (*SentimentReq) Descriptor() ([]byte, []int) {
	return file_sentiment_proto_rawDescGZIP(), []int{0}
}

func (x *SentimentReq) GetValue() string {
	if x != nil {
		return x.Value
	}
	return ""
}

type SentimentRep struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Score     int32    `protobuf:"zigzag32,1,opt,name=score,proto3" json:"score,omitempty"`
	Positives []string `protobuf:"bytes,2,rep,name=positives,proto3" json:"positives,omitempty"`
	Negatives []string `protobuf:"bytes,3,rep,name=negatives,proto3" json:"negatives,omitempty"`
}

func (x *SentimentRep) Reset() {
	*x = SentimentRep{}
	if protoimpl.UnsafeEnabled {
		mi := &file_sentiment_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SentimentRep) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SentimentRep) ProtoMessage() {}

func (x *SentimentRep) ProtoReflect() protoreflect.Message {
	mi := &file_sentiment_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SentimentRep.ProtoReflect.Descriptor instead.
func (*SentimentRep) Descriptor() ([]byte, []int) {
	return file_sentiment_proto_rawDescGZIP(), []int{1}
}

func (x *SentimentRep) GetScore() int32 {
	if x != nil {
		return x.Score
	}
	return 0
}

func (x *SentimentRep) GetPositives() []string {
	if x != nil {
		return x.Positives
	}
	return nil
}

func (x *SentimentRep) GetNegatives() []string {
	if x != nil {
		return x.Negatives
	}
	return nil
}

var File_sentiment_proto protoreflect.FileDescriptor

var file_sentiment_proto_rawDesc = []byte{
	0x0a, 0x0f, 0x73, 0x65, 0x6e, 0x74, 0x69, 0x6d, 0x65, 0x6e, 0x74, 0x2e, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x12, 0x09, 0x73, 0x65, 0x6e, 0x74, 0x69, 0x6d, 0x65, 0x6e, 0x74, 0x22, 0x24, 0x0a, 0x0c,
	0x53, 0x65, 0x6e, 0x74, 0x69, 0x6d, 0x65, 0x6e, 0x74, 0x52, 0x65, 0x71, 0x12, 0x14, 0x0a, 0x05,
	0x76, 0x61, 0x6c, 0x75, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x76, 0x61, 0x6c,
	0x75, 0x65, 0x22, 0x60, 0x0a, 0x0c, 0x53, 0x65, 0x6e, 0x74, 0x69, 0x6d, 0x65, 0x6e, 0x74, 0x52,
	0x65, 0x70, 0x12, 0x14, 0x0a, 0x05, 0x73, 0x63, 0x6f, 0x72, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28,
	0x11, 0x52, 0x05, 0x73, 0x63, 0x6f, 0x72, 0x65, 0x12, 0x1c, 0x0a, 0x09, 0x70, 0x6f, 0x73, 0x69,
	0x74, 0x69, 0x76, 0x65, 0x73, 0x18, 0x02, 0x20, 0x03, 0x28, 0x09, 0x52, 0x09, 0x70, 0x6f, 0x73,
	0x69, 0x74, 0x69, 0x76, 0x65, 0x73, 0x12, 0x1c, 0x0a, 0x09, 0x6e, 0x65, 0x67, 0x61, 0x74, 0x69,
	0x76, 0x65, 0x73, 0x18, 0x03, 0x20, 0x03, 0x28, 0x09, 0x52, 0x09, 0x6e, 0x65, 0x67, 0x61, 0x74,
	0x69, 0x76, 0x65, 0x73, 0x32, 0x52, 0x0a, 0x07, 0x41, 0x6e, 0x61, 0x6c, 0x79, 0x7a, 0x65, 0x12,
	0x47, 0x0a, 0x11, 0x47, 0x65, 0x74, 0x53, 0x65, 0x6e, 0x74, 0x69, 0x6d, 0x65, 0x6e, 0x74, 0x56,
	0x61, 0x6c, 0x75, 0x65, 0x12, 0x17, 0x2e, 0x73, 0x65, 0x6e, 0x74, 0x69, 0x6d, 0x65, 0x6e, 0x74,
	0x2e, 0x53, 0x65, 0x6e, 0x74, 0x69, 0x6d, 0x65, 0x6e, 0x74, 0x52, 0x65, 0x71, 0x1a, 0x17, 0x2e,
	0x73, 0x65, 0x6e, 0x74, 0x69, 0x6d, 0x65, 0x6e, 0x74, 0x2e, 0x53, 0x65, 0x6e, 0x74, 0x69, 0x6d,
	0x65, 0x6e, 0x74, 0x52, 0x65, 0x70, 0x22, 0x00, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_sentiment_proto_rawDescOnce sync.Once
	file_sentiment_proto_rawDescData = file_sentiment_proto_rawDesc
)

func file_sentiment_proto_rawDescGZIP() []byte {
	file_sentiment_proto_rawDescOnce.Do(func() {
		file_sentiment_proto_rawDescData = protoimpl.X.CompressGZIP(file_sentiment_proto_rawDescData)
	})
	return file_sentiment_proto_rawDescData
}

var file_sentiment_proto_msgTypes = make([]protoimpl.MessageInfo, 2)
var file_sentiment_proto_goTypes = []interface{}{
	(*SentimentReq)(nil), // 0: sentiment.SentimentReq
	(*SentimentRep)(nil), // 1: sentiment.SentimentRep
}
var file_sentiment_proto_depIdxs = []int32{
	0, // 0: sentiment.Analyze.GetSentimentValue:input_type -> sentiment.SentimentReq
	1, // 1: sentiment.Analyze.GetSentimentValue:output_type -> sentiment.SentimentRep
	1, // [1:2] is the sub-list for method output_type
	0, // [0:1] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_sentiment_proto_init() }
func file_sentiment_proto_init() {
	if File_sentiment_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_sentiment_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SentimentReq); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_sentiment_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SentimentRep); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_sentiment_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   2,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_sentiment_proto_goTypes,
		DependencyIndexes: file_sentiment_proto_depIdxs,
		MessageInfos:      file_sentiment_proto_msgTypes,
	}.Build()
	File_sentiment_proto = out.File
	file_sentiment_proto_rawDesc = nil
	file_sentiment_proto_goTypes = nil
	file_sentiment_proto_depIdxs = nil
}
