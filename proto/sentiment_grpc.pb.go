package sentiment

import (
	context "context"

	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// AnalyzeClient is the client API for Analyze service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type AnalyzeClient interface {
	GetSentimentValue(ctx context.Context, in *SentimentReq, opts ...grpc.CallOption) (*SentimentRep, error)
}

type analyzeClient struct {
	cc grpc.ClientConnInterface
}

func NewAnalyzeClient(cc grpc.ClientConnInterface) AnalyzeClient {
	return &analyzeClient{cc}
}

func (c *analyzeClient) GetSentimentValue(ctx context.Context, in *SentimentReq, opts ...grpc.CallOption) (*SentimentRep, error) {
	out := new(SentimentRep)
	err := c.cc.Invoke(ctx, "/sentiment.Analyze/GetSentimentValue", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// AnalyzeServer is the server API for Analyze service.
// All implementations must embed UnimplementedAnalyzeServer
// for forward compatibility
type AnalyzeServer interface {
	GetSentimentValue(context.Context, *SentimentReq) (*SentimentRep, error)
	mustEmbedUnimplementedAnalyzeServer()
}

// UnimplementedAnalyzeServer must be embedded to have forward compatible implementations.
type UnimplementedAnalyzeServer struct {
}

func (UnimplementedAnalyzeServer) GetSentimentValue(context.Context, *SentimentReq) (*SentimentRep, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetSentimentValue not implemented")
}
func (UnimplementedAnalyzeServer) mustEmbedUnimplementedAnalyzeServer() {}

// UnsafeAnalyzeServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to AnalyzeServer will
// result in compilation errors.
type UnsafeAnalyzeServer interface {
	mustEmbedUnimplementedAnalyzeServer()
}

func RegisterAnalyzeServer(s grpc.ServiceRegistrar, srv AnalyzeServer) {
	s.RegisterService(&Analyze_ServiceDesc, srv)
}

func _Analyze_GetSentimentValue_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SentimentReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AnalyzeServer).GetSentimentValue(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/sentiment.Analyze/GetSentimentValue",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AnalyzeServer).GetSentimentValue(ctx, req.(*SentimentReq))
	}
	return interceptor(ctx, in, info, handler)
}

// Analyze_ServiceDesc is the grpc.ServiceDesc for Analyze service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var Analyze_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "sentiment.Analyze",
	HandlerType: (*AnalyzeServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetSentimentValue",
			Handler:    _Analyze_GetSentimentValue_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "sentiment.proto",
}
