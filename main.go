package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"time"

	sentiment "gitlab.com/ayushsherpa111/simpleHttpServer/proto"

	"gitlab.com/ayushsherpa111/simpleHttpServer/chatroom"
	"gitlab.com/ayushsherpa111/simpleHttpServer/router"
	"gitlab.com/ayushsherpa111/simpleHttpServer/server"
	"gitlab.com/ayushsherpa111/simpleHttpServer/settings"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func init() {
	settings.Setup()
}

func NVL(val, fallback string) string {
	if len(val) == 0 {
		return fallback
	}
	return val
}

func main() {
	var (
		saSvc, saPort = NVL(os.Getenv("PROTO_SVC"), "localhost"), NVL(os.Getenv("PROTO_PORT"), "8086")
		hub           = chatroom.NewHub()
		conn, err     = grpc.Dial(saSvc+":"+saPort, grpc.WithTransportCredentials(insecure.NewCredentials()))
	)

	if err != nil {
		return
	}

	defer conn.Close()
	client := sentiment.NewAnalyzeClient(conn)
	fmt.Println(client)

	go hub.Run()

	r := router.InitRouter()

	r.Get("/test", func(rw http.ResponseWriter, r *http.Request) {
		rw.Header().Set("Content-Type", "application/json")
		fmt.Fprintf(rw, `{hello:"world"}`)
	})

	r.Get("/ws", func(rw http.ResponseWriter, r *http.Request) {
		chatroom.NewClient(hub, rw, r)
	})

	r.Options("/analyze", func(rw http.ResponseWriter, r *http.Request) {
		rw.WriteHeader(http.StatusNoContent)
	})

	r.Post("/analyze", func(rw http.ResponseWriter, r *http.Request) {
		var rpcPayload sentiment.SentimentReq = sentiment.SentimentReq{}

		rw.Header().Set("Content-Type", "application/json")
		rw.Header().Set("Access-Control-Allow-Headers", "*")
		rw.Header().Set("Access-Control-Allow-Origin", "http://localhost:3000")

		b, err := io.ReadAll(r.Body)
		if err != nil {
			return
		}

		err = json.Unmarshal(b, &rpcPayload)
		if err != nil {
			return
		}

		fmt.Println(rpcPayload.Value)

		ctx, cancel := context.WithTimeout(r.Context(), time.Second)
		defer cancel()
		reply, err := client.GetSentimentValue(ctx, &rpcPayload)

		if err != nil {
			fmt.Println(err.Error())
			fmt.Fprintf(rw, "{'OK':false}")
		} else {
			payload, _ := json.Marshal(reply)
			fmt.Fprintf(rw, string(payload))
		}

	})

	r.RegisterRoutes()

	srv := server.New(fmt.Sprintf(":%d", settings.Server.EndPoint), r)
	srv.ListenAndServe()
}
