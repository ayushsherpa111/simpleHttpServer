FROM golang:1.19.0-bullseye as builder
WORKDIR /home/simpleHttpServer

COPY . .

RUN go mod tidy
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo .


FROM alpine:latest

RUN apk --no-cache add ca-certificates

WORKDIR /root

COPY --from=builder /home/simpleHttpServer/simpleHttpServer .

EXPOSE 8080

ENV PROTO_PORT=54011 PROTO_SVC="rpc-svc"

CMD ["./simpleHttpServer"]
