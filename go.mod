module gitlab.com/ayushsherpa111/simpleHttpServer

go 1.16

require (
	github.com/golang/protobuf v1.5.2
	github.com/gorilla/websocket v1.5.0
	google.golang.org/grpc v1.50.1
	google.golang.org/protobuf v1.28.1
)
