package chatroom

import (
	"fmt"
	"time"
)

const (
	timeout = 10 * time.Second
)

type hub struct {
	clients map[*Client]bool

	register chan *Client

	unregister chan *Client

	broadcast chan []byte
}

func (h *hub) Run() {
	timeoutChan := time.After(timeout)
	for {
		select {
		case newClient := <-h.register:
			h.clients[newClient] = true
			fmt.Println(h.clients)
		case client := <-h.unregister:
			if _, ok := h.clients[client]; ok {
				h.clients[client] = false
				close(client.send)
			}
		case message := <-h.broadcast:
			for client, active := range h.clients {
				if active {
					select {
					case client.send <- message:
						fmt.Println("message received ", message)
					case <-timeoutChan:
						h.unregister <- client
					}
				}
			}
		}
	}
}

func NewHub() *hub {
	return &hub{
		clients:    make(map[*Client]bool),
		register:   make(chan *Client, 100),
		unregister: make(chan *Client),
		broadcast:  make(chan []byte),
	}
}
