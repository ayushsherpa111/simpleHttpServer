package chatroom

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
)

const (
	writeWait = time.Second * 10
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

type Client struct {
	conn *websocket.Conn

	send chan []byte

	hub *hub
}

func (c *Client) readBuf() {
	for {
		_, message, err := c.conn.ReadMessage()
		if err != nil {
			return
		}
		c.hub.broadcast <- message
	}
}

func (c *Client) writeBuf() {
	ticker := time.NewTicker(time.Second * 20)
	for {
		select {
		case message, ok := <-c.send:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				c.hub.unregister <- c
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}
			err := c.conn.WriteMessage(websocket.TextMessage, message)
			if err != nil {
				fmt.Println(err.Error())
			}
		case <-ticker.C:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
		}
	}
}

func NewClient(hub *hub, w http.ResponseWriter, req *http.Request) {
	conn, err := upgrader.Upgrade(w, req, nil)
	if err != nil {
		log.Fatalln(err.Error())
		return
	}

	c := &Client{
		hub:  hub,
		send: make(chan []byte),
		conn: conn,
	}

	hub.register <- c

	go c.readBuf()
	go c.writeBuf()
}
