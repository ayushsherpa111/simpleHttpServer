package settings

type dbSetting struct {
	Hostname string
	Port     int
}

var Database = &dbSetting{}
